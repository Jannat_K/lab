﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lab.Models
{
    public class Dish
    {
        public int Id { get; set; }
        public string Meal_name { get; set; }
        public int Price { get; set; }
        public string Meal_discription { get; set; }


        public int CafeId { get; set; }
        public virtual Cafe Cafe { get; set; }
    }
}
