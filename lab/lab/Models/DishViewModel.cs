﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lab.Models
{
    public class DishViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }
        public string Discription { get; set; }
        public Cafe Cafe { get; set; }
        public string DishName { get; set; }
        public List<Dish> Dishes { get; set; }
    }
}

